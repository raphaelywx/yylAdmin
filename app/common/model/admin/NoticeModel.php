<?php
// +----------------------------------------------------------------------
// | yylAdmin 前后分离，简单轻量，免费开源，开箱即用，极简后台管理系统
// +----------------------------------------------------------------------
// | Copyright https://gitee.com/skyselang All rights reserved
// +----------------------------------------------------------------------
// | Gitee: https://gitee.com/skyselang/yylAdmin
// +----------------------------------------------------------------------

// 公告管理模型
namespace app\common\model\admin;

use think\Model;
use hg\apidoc\annotation as Apidoc;

class NoticeModel extends Model
{
    // 表名
    protected $name = 'admin_notice';
    // 表主键
    protected $pk = 'admin_notice_id';

    /**
     * @Apidoc\Field("admin_notice_id")
     */
    public function id()
    {
    }

    /**
     * @Apidoc\WithoutField("type,intro,content,is_delete,update_time,delete_time")
     */
    public function listReturn()
    {
    }

    /**
     * 
     */
    public function infoReturn()
    {
    }

    /**
     * @Apidoc\WithoutField("admin_notice_id,admin_user_id,is_delete,create_time,update_time,delete_time")
     */
    public function addParam()
    {
    }

    /**
     * @Apidoc\WithoutField("admin_user_id,is_delete,create_time,update_time,delete_time")
     */
    public function editParam()
    {
    }

    /**
     * @Apidoc\Field("is_open")
     */
    public function is_open()
    {
    }

    /**
     * @Apidoc\Field("open_time_start")
     */
    public function open_time_start()
    {
    }

    /**
     * @Apidoc\Field("open_time_end")
     */
    public function open_time_end()
    {
    }
}
